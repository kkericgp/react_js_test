import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// the translations
// (tip move them in a JSON file and import them,
// or even better, manage them separated from your code: https://react.i18next.com/guides/multiple-translation-files)
const resources = {
  en: {
    translation: {
      "Login Prompt": "Please input user name and password:",
      "username": "User Name",
      "pw": "password",
      "Login": "Login",
      "Wrong IDPW": "Wrong ID/password!",
      "Home page": "Home Page",
      "Welcome": "Welcome",
      "Welcome message": "Welcome to the English version!",
      "Contact": "Contact",
      "Contact message": "Contact me",
      "Email": "Email: ",
      "phoneNo": "Mobile Phone: "
    }
  },
  cn: {
    translation: {
      "Welcome": "歡迎",
      "Login Prompt": "請輸入用戶名稱及密碼",
      "username": "用戶名稱",
      "pw": "密碼",
      "Login": "登入",
      "Wrong IDPW": "用戶名稱或密碼錯誤！請重新輸入",
      "Welcome message": "歡迎來到中文版本",
      "Home page": "主頁",
      "Contact": "聯絡",
      "Contact message": "聯絡方式",
      "Email": "電郵：",
      "phoneNo": "電話："
    }
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    compatibilityJSON: 'v3',
    resources,
    lng: "en", // language to use, more information here: https://www.i18next.com/overview/configuration-options#languages-namespaces-resources
    // you can use the i18n.changeLanguage function to change the language manually: https://www.i18next.com/overview/api#changelanguage
    // if you're using a language detector, do not define the lng option

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

  export default i18n;