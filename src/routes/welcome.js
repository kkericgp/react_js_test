import { t } from "i18next";

export default function Welcome() {
  return (
    <main style={{ padding: "1rem 0" }}>
      <h3>{t('Welcome message')}</h3>
    </main>
  );
}