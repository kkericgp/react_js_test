import { useTranslation } from 'react-i18next';
import { Outlet, Link } from "react-router-dom";

export default function Home() {
  const { t, i18n } = useTranslation();
  return (
    <div>
      <h2>{t('Home page')}</h2>
      <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem"
        }}
      >
        <Link to="/home/welcome">{t('Welcome')}</Link> |{'\t'}
        <Link to="/home/contact">{t('Contact')}</Link>
      </nav>
      <Outlet />
    </div>
  );
}