import { useTranslation } from 'react-i18next';

export default function About() {
  const { t, i18n } = useTranslation();
  return (
    <main style={{ padding: "1rem 0" }}>
      <h3>{t('Contact message')}</h3>
      <p>{t('Email')}kkericgp@gmail.com</p>
      <p>{t('phoneNo')}91541480</p>
    </main>
  );
}