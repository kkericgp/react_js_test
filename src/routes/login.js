import React from 'react';
import {useNavigate} from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';

export default function Login(props) {
  const [text_ID, onChangeID] = React.useState(null);
  const [text_PW, onChangePW] = React.useState(null);
  const navigate = useNavigate();
  const { t, i18n } = useTranslation();
  const [alignment, setAlignment] = React.useState(i18n.language);
  const handleChange = (event, newAlignment) => {
      setAlignment(newAlignment);
      i18n.changeLanguage(event.target.value);
    }

    return (
      <div style={appStyle}>
        <form style={formStyle} >
          <label style={labelStyle}>{t('username')}</label>
          <input 
            style={inputStyle}
            type='text' 
            value={text_ID} 
            onInput={e => onChangeID(e.target.value)}
          />
          <label style={labelStyle}>{t('pw')}</label>
          <input 
            style={inputStyle} 
            type='password'
            value={text_PW}
            onInput={e => onChangePW(e.target.value)}
          />
          <div>
            <button 
              style={submitStyle}
              onClick={() => {
                (text_ID === 'admin' && text_PW === 'Admin&8181')
                  ? navigate('/home/welcome')
                  : alert(t('Wrong IDPW'));
                }}
            >{t('Login')}</button>
          </div>
          <div>
            <ToggleButtonGroup
              style={{padding: '10px 0'}}
              size='small'
              color="primary"
              value={alignment}
              exclusive
              onChange={handleChange}
            >
              <ToggleButton value="cn">CN</ToggleButton>
              <ToggleButton value="en">EN</ToggleButton>
            </ToggleButtonGroup>
          </div>
        </form>
      </div>
    );
};

const appStyle = {
  height: '250px',
  display: 'flex'
};

const formStyle = {
  margin: 'auto',
  padding: '10px',
  border: '1px solid #c9c9c9',
  borderRadius: '5px',
  background: '#f5f5f5',
  width: '220px',
  display: 'block'
};

const labelStyle = {
  margin: '10px 0 5px 0',
  fontFamily: 'Arial, Helvetica, sans-serif',
  fontSize: '15px',
};

const inputStyle = {
  margin: '5px 0 10px 0',
  padding: '5px', 
  border: '1px solid #bfbfbf',
  borderRadius: '3px',
  boxSizing: 'border-box',
  width: '100%'
};

const submitStyle = {
  margin: '10px 0 0 0',
  padding: '7px 10px',
  border: '1px solid #efffff',
  borderRadius: '3px',
  background: '#3085d6',
  width: '100%', 
  fontSize: '15px',
  color: 'white',
  display: 'block'
};
