import { render } from "react-dom";
import {
  BrowserRouter,
  Navigate,
  Routes,
  Route
} from "react-router-dom";
import Home from "./routes/home"
import Login from "./routes/login";
import Welcome from "./routes/welcome";
import Contact from "./routes/contact"
import './i18n';

const rootElement = document.getElementById("root");
render(
  <BrowserRouter>
    <Routes>
    <Route path="/login" element={<Login />} />
      <Route path="/home" element={<Home />}>
        <Route path="welcome" element={<Welcome />} />
        <Route path="contact" element={<Contact />} />
      </Route>
      <Route index element={<Navigate to="/login" />} />
    </Routes>
  </BrowserRouter>,
  rootElement
);